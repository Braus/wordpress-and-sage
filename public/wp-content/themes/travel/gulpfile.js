var gulp = require('gulp');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-ruby-sass');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var browserSync  = require('browser-sync').create();

gulp.task('sass', function () {
    return sass('./assets/styles/*.scss', {
            sourcemap: true,
            lineNumbers: true

    } )
        .on('error', sass.logError)
        .pipe(sourcemaps.write())
        .pipe(sourcemaps.write('maps', {
            includeContent: false,
            sourceRoot: './dist'
        }))
        .pipe(gulp.dest('./dist/styles'))
        .pipe(browserSync.stream())
        .pipe(notify({ message: "Sassyado" }));
});

gulp.task('js', function(){
         gulp.src('./assets/scripts/*.js')
        .pipe(uglify())
        .pipe(concat("app.min.js"))
        .pipe(gulp.dest('./dist/scripts/'))
        .pipe(browserSync.stream())
        .pipe( notify({ message: "Javascript is now ugly!"}) );
});

gulp.task('watch', function(){
        browserSync.init({
            files: ['{lib,templates}/**/*.php', '*.php'],
            //proxy: config.devUrl,
            proxy: "http://travel.dev",
            snippetOptions: {
                whitelist: ['/wp-admin/admin-ajax.php'],
                blacklist: ['/wp-admin/**']
            }
        });
    // watch scss files
    gulp.watch(['./assets/styles/**/*.scss'], ['sass'] );
    gulp.watch(['./assets/scripts/*.js'], ['js'] );
    gulp.watch(['bower.json', 'assets/manifest.json'], ['build'])

});

gulp.task('default', ['sass', 'js', 'watch']);

gulp.task('dev', ['sass', 'js', 'watch']);