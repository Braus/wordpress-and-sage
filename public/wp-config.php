<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */




if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp');
}
if (!defined('WP_HOME')) {
	define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST']);
}



/* use wp-content from root directory  */
if (!defined('WP_CONTENT_DIR')) {
	define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
}
if (!defined('WP_CONTENT_URL')) {
	define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content' );
}


// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {

	define( 'WP_LOCAL_DEV', true );
	define( 'WP_STAGING_DEV', false );
	include( dirname( __FILE__ ) . '/wp-config-local.php' );

} elseif (file_exists( dirname( __FILE__ ) . '/wp-config-staging.php' ) ) {

	define( 'WP_LOCAL_DEV', false );
	define( 'WP_STAGING_DEV', true );
	include( dirname( __FILE__ ) . '/wp-config-staging.php' );

} else {
	define( 'WP_LOCAL_DEV', false );
	define( 'WP_STAGING_DEV', false );

	// ** MySQL settings - You can get this info from your web host ** //
	/** The name of the database for WordPress */
	define('DB_NAME', 'database_name_here');

	/** MySQL database username */
	define('DB_USER', 'username_here');

	/** MySQL database password */
	define('DB_PASSWORD', 'password_here');

	/** MySQL hostname */
	define('DB_HOST', 'localhost');

	/** Database Charset to use in creating database tables. */
	define('DB_CHARSET', 'utf8');

	/** The Database Collate type. Don't change this if in doubt. */
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%oQ.A&p+@dY5h0jP*VJ2e+$# |>{}reF0f.i.3zV1;ZL`=O>c)z{vP#{#o0U%J;r');
define('SECURE_AUTH_KEY',  '3I~B5A<.28JR -Ba^;{^VpY7xB1CM91ET[c$Un=f|d.j1`X0V_<2e=dT&au 5H^V');
define('LOGGED_IN_KEY',    ',e#9Fm.(@ZuR(Sut!@0_VRa/2q-3A`Ox4?oJ[+5DXa9kcucPM)7vs=5~;)m_=v/P');
define('NONCE_KEY',        ')Z;DBJ^u&rj@?RI1-Z+jntFAGPX9i)X3?3kKPYS&}t(^n80|>pb}Sl4?_Y3w%^bK');
define('AUTH_SALT',        'PE8g6+p.X} t!&{<:(#&Ugt5Ay5tbJ[f;zdWhW])5X28p^>%Xj{AP,~q-`W?/KLl');
define('SECURE_AUTH_SALT', 'j^@-w>v|kYXlnjRWIcd3<j]0A^(aHcdEBS%T-iO(}#1H24{*gu]lH*2??DeTI;R[');
define('LOGGED_IN_SALT',   '++|-.{nrSw>36 RdD&|B.oKnB{BX@Tz.*FDf++2Zru{S<2*[![*,KiRqxu8{Dey|');
define('NONCE_SALT',       'Gq01.Bs|4W.+87mA<|Og+?(Q&/oWR,`.^4ZLHz?g^7|-f8?-unBORjn: dIS01+_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
